<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>您好Springboot</title>
<!-- 导入函数类库 -->
<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
<!-- 开始执行ajax -->
<script type="text/javascript">
	$(function(){
		
		//$.getJSON   $.load()加载html代码片段
		
		$.ajax({
			type : "get",
			url  : "/userAjax",
			async : true,  //默认是异步状态
			error: function(data){
				alert("当前服务器异常");
			},
			//data : {"id":100,"name":"tomcat"},
			data : "id=100&name=tomcat",
			success : function(data){
				let tr = '';
				for(let user of data){
					let id = user.id
					let name = user.name
					let age = user.age
					let sex = user.sex
					tr += "<tr align='center'><td>"+id+"</td><td>"+name+"</td><td>"+age+"</td><td>"+sex+"</td></tr>"
				}
				$("#tab1").append(tr)
			}
		});
		
		
		
		
		
		
		/**
		 * 遍历方式1
		 * for(let i=0;i<data.length;i++){
				let user = data[i];
				console.log(user.name);
			}
			
			for(let index in data){	//index代表下标
				console.log(data[index])
			}
		 */
		
		
		//1.利用$.get方式获取数据别表
		//$.get(1.url网址,2.传递的参数,3.回调函数,4.返回值类型)
		$.get("/userAjax3",function(data){
			let tr = '';
			//遍历返回值结果
			for(let user of data){
				let id = user.id
				let name = user.name
				let age = user.age
				let sex = user.sex
				tr += "<tr align='center'><td>"+id+"</td><td>"+name+"</td><td>"+age+"</td><td>"+sex+"</td></tr>"
			}
			//将tr标签追加到表格中
			//2.利用$.ajax方式获取数据列表
			$("#tab1").append(tr)
		});
	})
</script>
</head>
<body>
	<table border="1px" width="65%" align="center" id="tab1">
		<tr>
			<td colspan="6" align="center"><h3>学生信息</h3></td>
		</tr>
		<tr>
			<th>编号</th>
			<th>姓名</th>
			<th>年龄</th>
			<th>性别</th>
		</tr>
	</table>
</body>
</html>