package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName //如果表名与对象名称一致,则可以省略不写
public class User {
    @TableId(type = IdType.AUTO)    //主键自增
    private Integer id;
    private String name;
    private Integer age;
    private String sex;
}
