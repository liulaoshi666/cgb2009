package com.jt;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.management.Query;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
class SpringbootDemo2ApplicationTests {

	@Autowired
	private UserMapper userMapper;

	@Test
	public void test01(){
		List<User> userList = userMapper.findAll();
		System.out.println(userList);
	}


	@Test
	public void testInsert(){
		User user = new User();
		user.setName("鲁班七号").setAge(3).setSex("男");
		userMapper.insert(user);
	}

	/**
	 * 测试1:  查询id=5的数据
	 * 测试2:  根据name="唐僧"查询数据
	 */
	@Test
	public void testSelect(){
		User user = userMapper.selectById(5);
		System.out.println(user);
		//查询所有数据
		List<User> userList = userMapper.selectList(null);
		System.out.println(userList);

		//3.查询name="唐僧"数据 where name ="xxx"
		//常见关系运算符 = eq , > gt ,< lt, >= ge ,<= le
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("name", "唐僧");
		List<User> userList2 = userMapper.selectList(queryWrapper); //where条件构造器
		System.out.println(userList2);
	}

	/**
	 * 查询: id= 1,3,5,6的数据
	 * Sql:  select xxxxx where id in (1,3,5,6)
	 */
	@Test
	public void testSelect02(){
		Integer[] ids = {1,3,5,6}; //模拟前端传递的数据
		//一般需要将数组.转化为List集合
		List<Integer> idList = Arrays.asList(ids);
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.in("id", idList);
		System.out.println(userMapper.selectList(queryWrapper));
		//方式2: 直接批量查询
		System.out.println(userMapper.selectBatchIds(idList));
	}

	/**
	 * 条件: 查询名字中包含'精'字的数据
	 * sql:  select ..... where name like '%精%'
	 * 									   '王%'
	 */
	@Test
	public void testSelect03(){
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.like("name", "精");
		System.out.println(
				userMapper.selectList(queryWrapper));

		//2.模糊查询2
		QueryWrapper<User> queryWrapper2 = new QueryWrapper<>();
		queryWrapper2.likeRight("name", "王");
		System.out.println(userMapper.selectList(queryWrapper2));

	}

	/**
	 *
	 * 将数据按照age降序排列,如果年龄相同 按照ID降序排列
	 */
	@Test
	public void testSelect04(){
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.orderByDesc("age","id");
		System.out.println(userMapper.selectList(queryWrapper));
	}

	/**
	 * 查询name中包含"君"字 or sex为女性 ,按照Id降序排列
	 * sql: where name like "%君%" and xxxx
	 */
	@Test
	public void testSelect05(){
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.like("name", "君")
					.or()	//默认是and 如果需要调用or
					.eq("sex", "女")
					.orderByDesc("id");
		System.out.println(userMapper.selectList(queryWrapper));

	}

	/**
	 * 将name="测试案例" 修改为 "测试环境"
	 * 参数说明: entity 需要改为的数据对象
	 * 			 updateWrapper 更新条件构造器
	 * 特点: 将对象中不为null的属性当做set条件	 */
	@Test
	public void testUpdate(){
		User user = new User();
		user.setName("测试环境");
		UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
		updateWrapper.eq("name", "测试案例");
		userMapper.update(user,updateWrapper);
	}



}
