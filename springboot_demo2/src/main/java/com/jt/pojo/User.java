package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data     //get/set/toString/equals
@Accessors(chain = true)    //实现链式加载
@AllArgsConstructor         //全参构造
@NoArgsConstructor          //无参构造
@TableName("user")          //1.关联表名
public class User {
    //对象与数据表要完美映射  对象名称与表名  对象的属性与表的字段
    //注意对象使用包装类型
    @TableId(type = IdType.AUTO) //主键自增
    private Integer id;   //id默认值0 null
    //@TableField(value = "name") 可以省略,但是名称必须相同
    private String name;
    //@TableField(value = "age")
    private Integer age;
    //@TableField(value = "sex")
    private String sex;

   /* public User setId(Integer id){
        this.id = id;
        return this;
    }

    public User setName(String name){
        this.name = name;
        return this;
    }*/
}
