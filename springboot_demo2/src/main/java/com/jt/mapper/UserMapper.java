package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
//继承公共的mapper接口方法
@Mapper
public interface UserMapper extends BaseMapper<User> {

    List<User> findAll(); //查询全部的用户记录
}
