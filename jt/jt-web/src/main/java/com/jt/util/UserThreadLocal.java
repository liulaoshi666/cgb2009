package com.jt.util;

import com.jt.pojo.User;

public class UserThreadLocal {

    private static ThreadLocal<User> userThreadLocal = new ThreadLocal<>();

    //存数据
    public static void set(User user){

        userThreadLocal.set(user);
    }

    //取数据
    public static User get(){

        return userThreadLocal.get();
    }

    //数据删除  防止内存泄露
    public static void remove(){

        userThreadLocal.remove();
    }
}
