package com.jt.controller;

import com.jt.mapper.ItemMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.EasyUITable;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jt.service.ItemService;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/item")
public class ItemController {
	
	@Autowired
	private ItemService itemService;

	/**
	 * 1.实现商品类标展现
	 * url地址: http://localhost:8091/item/query?page=1&rows=20
	 * 参数:	page页数    rows 行数
	 * 返回值:  EasyUITable
	 */
	@RequestMapping("/query")
	public EasyUITable findItemByPage( int page, int rows){

		return itemService.findItemByPage(page,rows);
	}


	/**
	 * 实现商品新增操作
	 * url地址: http://localhost:8091/item/save
	 * 参数:    form表单的数据  item数据  /itemDesc数据
	 * 返回值:  SysResult对象
	 */
	@RequestMapping("/save")
	public SysResult saveItem(Item item,ItemDesc itemDesc){

		itemService.saveItem(item,itemDesc);
		return SysResult.success();
	}

	/**
	 * 实现商品更新操作
	 * url : http://localhost:8091/item/update
	 * 参数: 整个form表单  Item对象接收
	 * 返回值: SysResult对象
	 */
	@RequestMapping("/update")
	public SysResult updateItem(Item item,ItemDesc itemDesc){

		itemService.updateItem(item,itemDesc);
		return SysResult.success();
	}

	/**
	 * 商品删除
	 * url : http://localhost:8091/item/delete
	 * 参数 :  ids=100,101,102
	 * 返回值:  SysResult对象
	 * 3分钟完成
	 * SpringMVC: 如果参数中有,号 则可以自动的实现数组的转化
	 */
	@RequestMapping("/delete")
	public SysResult deleteItems(Long... ids){

		itemService.deleteItems(ids);
		return SysResult.success();
	}

	/**
	 * 能否利用一个方法实现 商品的上架/下架操作
	 * URL1:  	/item/2         下架操作	status=2
	 * URL2: 	/item/1		  上架操作   status=1
	 */
	@RequestMapping("/{status}")
	public SysResult updateStatus(Long[] ids,@PathVariable Integer status){

		itemService.updateStatus(ids,status);
		return SysResult.success();
	}

	/**
	 * 业务说明:
	 * 	 根据id获取商品详情信息
	 *
	 * URL: 	http://localhost:8091/item/query/item/desc/1474392043
	 * 参数: 	1474392043 商品ID
	 * 返回值:   SysResult对象
	 */
	 @RequestMapping("/query/item/desc/{itemId}")
	 public SysResult findItemDescById(@PathVariable Long itemId){

	 	ItemDesc itemDesc = itemService.findItemDescById(itemId);
	 	return SysResult.success(itemDesc);
	 }



}
