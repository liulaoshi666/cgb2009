package com.jt.controller;

import com.jt.pojo.ItemCat;
import com.jt.service.ItemCatService;
import com.jt.vo.EasyUITree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/item/cat")   //bean的ID   itemCatController
public class ItemCatController {

    //注入  1.通过Bean的ID进行注入
    //     2. 按照类型注入.   Spring中规定 一般的接口都是单实现
    @Autowired
    //@Qualifier("itemCatServiceImpl")
    private ItemCatService itemCatService;

    /**
     *  业务需求:  根据id查询商品分类信息
     *  url地址:   http://localhost:8091/item/cat/queryItemName?itemCatId=440
     *  参数:      itemCatId=440
     *  返回值:    返回商品分类名称
     */


    /**
     * SpringMVC 参数传递的本质
     * 问题:  itemCatId是如何当做参数进行的传递,框架如何实现???
     * @param itemCatId
     * @return
     *
     * <input  type="text" name="itemCatId"  value="100" />
     */

    @RequestMapping("/queryItemName")
    public String findItemCatName(Long itemCatId){
        ItemCat itemCat =
                        itemCatService.findItemCatName(itemCatId);
        return itemCat.getName();
    }

    /**
     * 实现商品分类树形结构展现
     *  1.   url地址        /item/cat/list
     *  2.  请求参数        parent_id=0
     *  3.  返回值结果      List<EasyUITree>
     *  http://localhost:8091/item/cat/list?id=1
     *
     */
    @RequestMapping("list")
    public List<EasyUITree> findItemCatList(Long id){

        long parentId = (id==null)?0:id;
        //从数据库中获取数据
        return itemCatService.findItemCatList(parentId);
        //从缓存中获取数据
        //return itemCatService.findItemCatCache(parentId);
    }














}
