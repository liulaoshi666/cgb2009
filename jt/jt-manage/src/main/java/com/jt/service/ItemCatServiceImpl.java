package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.annotation.CacheFind;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import com.jt.util.ObjectMapperUtil;
import com.jt.vo.EasyUITree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

@Service     //itemCatServiceImpl
public class ItemCatServiceImpl implements ItemCatService{

    @Autowired
    private ItemCatMapper itemCatMapper;
    //未来该数据可能被优化,所以添加false属性.
    @Autowired(required = false) //暂时不注入 调用时才注入 该对象可以没有
    //@Lazy   //要求对象必须有
    private Jedis jedis;


    @Override
    @CacheFind(key = "ITEM_CAT_ID")
    public ItemCat findItemCatName(Long itemCatId) {

        return itemCatMapper.selectById(itemCatId);
    }

    /**
     * 1.根据parentId 查询数据库记录
     * 2.将数据库的List集合转化为VO对象的集合.
     *
     * @param parentId
     * @return
     */
    @CacheFind(key = "ITEM_CAT_PARENT") //添加自定义注解 添加业务名称
    @Override
    public List<EasyUITree> findItemCatList(Long parentId) {
        List<EasyUITree> treeList = new ArrayList<>();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("parent_id", parentId);
        List<ItemCat> catList = itemCatMapper.selectList(queryWrapper);

        for (ItemCat itemCat : catList){
            long id = itemCat.getId();
            String text = itemCat.getName();
            String state = itemCat.getIsParent()?"closed":"open";
                            //是父级先关闭用户需要再打开
            EasyUITree easyUITree = new EasyUITree(id,text,state);
            treeList.add(easyUITree);
        }
        return treeList;
    }

    /**
     * 缓存处理业务逻辑:
     *     1.第一次查询先查缓存
     *     2.结果:
     *           有结果:    直接从缓存中获取
     *           没有结果:  第一次查询,先查询数据库,之后保存到缓存中
     *     3.Redis.set(0,json)
     *     @param parentId
     * @return
     */
    @Override
    public List<EasyUITree> findItemCatCache(long parentId) {
        Long startTime = System.currentTimeMillis();
        String key = "ITEM_CAT_PARENTID::"+parentId;
        List<EasyUITree> treeList = new ArrayList<>();
        if(jedis.exists(key)){
            //表示数据存在
            String json = jedis.get(key);
            treeList = ObjectMapperUtil.toObj(json, treeList.getClass());
            Long endTime = System.currentTimeMillis();
            System.out.println("查询缓存的时间:"+(endTime-startTime)+"毫秒");
        }else{
            //表示数据不存在,需要查询数据库
            treeList = findItemCatList(parentId);
            //将结果保存到缓存中
            String json = ObjectMapperUtil.toJSON(treeList);
            jedis.set(key, json);
            Long endTime = System.currentTimeMillis();
            System.out.println("查询数据库的时间:"+(endTime-startTime)+"毫秒");
        }
        return treeList;
    }

}
