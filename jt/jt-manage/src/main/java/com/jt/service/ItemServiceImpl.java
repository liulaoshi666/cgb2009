package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.EasyUITable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jt.mapper.ItemMapper;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	private ItemMapper itemMapper;
	@Autowired
	private ItemDescMapper itemDescMapper;

	@Override
	public EasyUITable findItemByPage(int page, int rows) {
		IPage<Item> iPage = new Page<>(page,rows);
		QueryWrapper queryWrapper = new QueryWrapper();
		queryWrapper.orderByDesc("updated");
		//使用接口 包装分页数据
		iPage = itemMapper.selectPage(iPage,queryWrapper);

		long total = iPage.getTotal();
		List<Item> itemList = iPage.getRecords();
		return new EasyUITable(total,itemList);
	}

	/**
	 * Item和ItemDesc应该同时入库,
	 * 难点: 如何获取主键ID
	 * 解决方案: 设置主键自增之后动态的回显给对象
	 * @param item
	 * @param itemDesc
	 */
	@Override
	@Transactional	//控制数据库实物
	public void saveItem(Item item, ItemDesc itemDesc) {
		item.setStatus(1);
		itemMapper.insert(item);

		//商品详情入库  MP会自动的回显!!!
		itemDesc.setItemId(item.getId());
		itemDescMapper.insert(itemDesc);
	}

	@Override
	@Transactional	//控制数据库实物
	public void updateItem(Item item, ItemDesc itemDesc) {

		itemMapper.updateById(item);
		itemDesc.setItemId(item.getId());
		itemDescMapper.updateById(itemDesc);
	}

	/*同时删除2张表数据*/
	@Override
	@Transactional
	public void deleteItems(Long[] ids) {
		//1.mp方式进行操作
		//itemMapper.deleteBatchIds(Arrays.asList(ids));

		//2.通过手写Sql方式实现数据删除?
		itemMapper.deleteItems(ids);
		itemDescMapper.deleteBatchIds(Arrays.asList(ids));
	}

	//作业:手写Sql	完成更新操作
	@Override
	public void updateStatus(Long[] ids, Integer status) {
		Item item = new Item();
		item.setStatus(status);
		UpdateWrapper updateWrapper = new UpdateWrapper();
		updateWrapper.in("id", Arrays.asList(ids));
		itemMapper.update(item,updateWrapper);
	}

	@Override
	public ItemDesc findItemDescById(Long itemId) {

		return itemDescMapper.selectById(itemId);
	}


	/**
	 * 业务需求:	1.分页查询数据库记录
	 * 			2.封装VO对象
	 * 作业1:  手写分页的方式实现分页查询  sql
	 * 作业2:  查看官网API 实现MP分页查询
	 *
	 * 分页sql:  每页20条记录
	 * 	select * from tb_item limit 起始位置,每页行数.
	 * 第一页:
	 * 	select * from tb_item limit 0,20    0-19
	 * 第二页:
	 * 	select * from tb_item limit 20,20
	 * 第N页:
	 * 	select * from tb_item limit (page-1)*rows,rows
	 * @param page
	 * @param rows
	 * @return
	 */
	/*@Override
	public EasyUITable findItemByPage(int page, int rows) {
		//1.获取记录总数
		long total = itemMapper.selectCount(null);
		//2.手写分页
		int start = (page - 1) * rows;
		List<Item> itemList = itemMapper.findItemByPage(start,rows);

		return new EasyUITable(total,itemList);
	}*/
}
