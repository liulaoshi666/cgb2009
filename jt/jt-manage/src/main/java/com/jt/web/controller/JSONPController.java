package com.jt.web.controller;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.jt.pojo.ItemDesc;
import com.jt.util.ObjectMapperUtil;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownServiceException;

@RestController
@CrossOrigin
public class JSONPController {

    /**
     * 接收jt-web的请求
     *  url地址:http://manage.jt.com/web/testJSONP?callback=jQuery111109613181307511978_1610351841976&_=1610351841977
     *  参数:   回调函数
     *  返回值结果: callback(json)
     */
    //@RequestMapping("/web/testJSONP")
    public String jsonp(String callback){
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("desc属性");
        String json = ObjectMapperUtil.toJSON(itemDesc);
        return callback + "(" + json +")";
    }

    //@RequestMapping("/web/testJSONP")
    public JSONPObject jsonp2(String callback){
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("AAAA");
        return new JSONPObject(callback,itemDesc);
    }

    @RequestMapping("/web/testJSONP")
    @CrossOrigin(value = "http://www.jt.com",methods = {RequestMethod.GET,RequestMethod.POST})    //标识某个网址允许跨域
    //@CrossOrigin    //所有的请求都允许跨域
    public ItemDesc itemDesc(){
        ItemDesc itemDesc = new ItemDesc();
        itemDesc.setItemId(100L).setItemDesc("AAAA");
        return itemDesc;
    }
}
