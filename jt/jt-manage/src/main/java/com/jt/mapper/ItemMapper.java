package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.Item;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ItemMapper extends BaseMapper<Item>{

    @Select("select * from tb_item order by updated desc limit #{start},#{rows}")
    List<Item> findItemByPage(int start, int rows);

    /*
        1.可以将数据封装为map集合  key=ids  value 数组
        2.Mybatis中只允许单值传参,
        3.如果遇到多值传参,一般将多值封装为单值  利用Map集合封装
          高版本的条件中 一般都是自动封装
    * */
    void deleteItems(Long[] ids);
}
