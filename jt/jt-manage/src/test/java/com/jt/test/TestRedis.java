package com.jt.test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.*;
import redis.clients.jedis.params.SetParams;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//@SpringBootTest
public class TestRedis {

    @Autowired
    private Jedis jedis;

    @Test
    public void testRedis(){
        //利用容器动态获取对象之后操作redis
        jedis.set("redis", "整合测试");
        System.out.println(jedis.get("redis"));
    }


    /**
     * 如果报错:
     *      1.防火墙
     *      2.redis配置文件   3处
     *      3.redis启动方式   redis-server redis.conf
     */
    @Test
    public void testString(){
        String host = "192.168.126.129";
        int port = 6379;
        Jedis jedis = new Jedis(host,port);
        jedis.set("redis", "你好Redis");
        System.out.println(jedis.get("redis"));
    }

    /**
     * 需求: 判断数据是否存在
     *      如果存在 则删除数据
     *      如果不存在则赋值操作
     */
    @Test
    public void test01(){
        Jedis jedis = new Jedis("192.168.126.129", 6379);
        if(jedis.exists("redis")){
            jedis.del("redis");
        }else {
            jedis.set("hello", "你好redis");
            System.out.println(jedis.get("hello"));
        }
    }

    //如果数据存在 不做任何操作,如果数据不存在则赋值
    @Test
    public void test02(){
        Jedis jedis = new Jedis("192.168.126.129", 6379);
        jedis.flushAll();
        //jedis.set("redis", "AAA");
        jedis.setnx("redis","BBB");
        System.out.println(jedis.get("redis"));
    }

    //添加一个数据,并且设定超时时间 10秒
    //设定超时时间应该注意
    // 原子性: 要么同时成功/要么同时失败.
    @Test
    public void test03() throws InterruptedException {
        Jedis jedis = new Jedis("192.168.126.129", 6379);
        //jedis.set("a","设定超时时间");
        //jedis.expire("a", 10);
        jedis.setex("a", 10, "设定超时时间");
        Thread.sleep(2000);
        System.out.println("剩余时间" + jedis.ttl("a"));
    }

    /**
     * 1.如果数据存在,则不允许修改  反之允许修改
     * 2.同时为数据设定超时时间. 10秒
     * 3.上述操作需要满足原子性要求
     *  XX: 存在才修改
     *  NX: 不存在才修改
     *  PX: 毫秒
     *  EX: 秒
     */
    @Test
     public void test04(){
         Jedis jedis = new Jedis("192.168.126.129", 6379);
         SetParams setParams = new SetParams();
         setParams.nx().ex(10);
         jedis.set("b", "xxxx",setParams);
         System.out.println(jedis.get("b"));
     }

    @Test
    public void test05(){
        Jedis jedis = new Jedis("192.168.126.129", 6379);
        jedis.hset("user", "id", "100");
        jedis.hset("user", "name", "user信息");
        jedis.hset("user", "age", "18");
        System.out.println(jedis.hgetAll("user"));
    }

    @Test
    public void test06(){
        Jedis jedis = new Jedis("192.168.126.129", 6379);
        //jedis.lpush("list","1,2,3,4,5");
        jedis.lpush("list","1","2","3","4","5");
        System.out.println(jedis.rpop("list"));
    }


    /**
     * 实现Redis分片机制测试
     * 思考: 数据如何存储???
     */
    @Test
    public void testShards(){
        List<JedisShardInfo> shards = new ArrayList<>();
        shards.add(new JedisShardInfo("192.168.126.129", 6379));
        shards.add(new JedisShardInfo("192.168.126.129", 6380));
        shards.add(new JedisShardInfo("192.168.126.129", 6381));
        ShardedJedis shardedJedis = new ShardedJedis(shards);
        shardedJedis.set("shards", "测试分片机制");
        System.out.println(shardedJedis.get("shards"));
    }


    /**
     * 实现redis哨兵测试
     */
    @Test
    public void testSentinel(){
        //2.指定连接池大小
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMinIdle(5);    //设定最小的空闲数量
        poolConfig.setMaxIdle(10);   //设定最大空闲数量
        poolConfig.setMaxTotal(100); //最大链接数

        //1.链接哨兵的集合
        Set<String> sentinels = new HashSet<>();
        sentinels.add("192.168.126.129:26379");
        JedisSentinelPool pool
                = new JedisSentinelPool("mymaster",sentinels,poolConfig);
        Jedis jedis = pool.getResource(); //获取资源
        jedis.set("aaa", "aaaaaaa");
        System.out.println(jedis.get("aaa"));
        jedis.close();  //关闭资源
    }

    //redis数据存储到了哪个节点中???
    @Test
    public void testCluster(){
        Set<HostAndPort> nodes = new HashSet<>();
        nodes.add(new HostAndPort("192.168.126.129", 7000));
        nodes.add(new HostAndPort("192.168.126.129", 7001));
        nodes.add(new HostAndPort("192.168.126.129", 7002));
        nodes.add(new HostAndPort("192.168.126.129", 7003));
        nodes.add(new HostAndPort("192.168.126.129", 7004));
        nodes.add(new HostAndPort("192.168.126.129", 7005));
        JedisCluster jedisCluster = new JedisCluster(nodes);
        jedisCluster.set("cluster", "哈哈哈");
        System.out.println(jedisCluster.get("cluster"));
    }


}
