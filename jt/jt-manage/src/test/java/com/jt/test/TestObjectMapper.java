package com.jt.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.util.ObjectMapperUtil;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestObjectMapper {

    /**
     * 1.通过测试类 实现对象与JSON之间的转化
     * 重点知识:
     *  1.对象转化JSON 获取所有的getXXX()方法~~~~去除get~~~~~首字母小写~~形成属性
     *  2.JSON转化为对象  利用Class的反射机制实例化对象~~~~获取json中的属性
     *                    ~~~~拼接setXXX方法~~~~~调用对象的setXXX(arg)方法为对象赋值    */
     @Test
     public void test01() throws JsonProcessingException {
         ItemDesc itemDesc = new ItemDesc();
         itemDesc.setItemId(100L).setItemDesc("转化测试").setCreated(new Date());
         ObjectMapper objectMapper = new ObjectMapper();
         //1.将对象转化为JSON
         String json = objectMapper.writeValueAsString(itemDesc);
         System.out.println(json);
         //2.将JSON转化为对象     反射思想
         ItemDesc itemDesc2 = objectMapper.readValue(json, ItemDesc.class);
         System.out.println(itemDesc2.getCreated());
     }

     @Test
     public void testList() throws JsonProcessingException {
         List<ItemDesc> list = new ArrayList<>();
         list.add(new ItemDesc().setItemId(100L).setItemDesc("案例1"));
         list.add(new ItemDesc().setItemId(100L).setItemDesc("案例1"));
         ObjectMapper objectMapper = new ObjectMapper();
         String json = objectMapper.writeValueAsString(list);
         System.out.println(json);
         List list2 = objectMapper.readValue(json, list.getClass());
         System.out.println(list2);
     }

     //完成工具API测试
     @Test
     public void testObject(){
         //1.对象转化为JSON
         ItemDesc itemDesc = new ItemDesc().setItemDesc("aaa").setItemId(100L);
         String json = ObjectMapperUtil.toJSON(itemDesc);
         System.out.println(json);

         //2.json转化对象
         ItemDesc itemDesc2 = ObjectMapperUtil.toObj(json, ItemDesc.class);
         System.out.println(itemDesc2);
     }

}
