package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jt.mapper.UserMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

	private static Map<Integer,String> columnMap = new HashMap<>();
	static  {
		columnMap.put(1, "username");
		columnMap.put(2, "phone");
		columnMap.put(3, "eamil");
	}

	
	@Autowired
	private UserMapper userMapper;


	@Override
	public List<User> findAll() {
		return userMapper.selectList(null);
	}

	//true 数据已经存在       false 数据可以使用
	//判断依据:  查询数据库 selectCount
	//Sql: select count(*) from tb_user where username = #{param}
	@Override
	public boolean checkUser(String param, Integer type) {
		QueryWrapper<User> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq(columnMap.get(type), param);
		int count = userMapper.selectCount(queryWrapper);
		//return count>0?true:false;
		return count > 0;
	}
}
