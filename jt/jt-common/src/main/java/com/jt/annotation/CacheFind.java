package com.jt.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME) //运行期有效
@Target(ElementType.METHOD)         //对方法有效
public @interface CacheFind {
    String key();               //要求用户必须指定key
    int seconds() default 0;    //无需设定超时时间
}
