package com.jt.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieUtils {

    /**
     * 1.保存cookie
     * path: 一般都是/
     */
    public static void addCookie(HttpServletResponse response,String cookieName, String cookieValue, String domain, Integer seconds){
        Cookie cookie = new Cookie(cookieName, cookieValue);
        cookie.setDomain(domain);
        cookie.setMaxAge(seconds);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    /**
     * 2.获取cookie
     */
    public static Cookie getCookie(HttpServletRequest request,String cookieName){
        Cookie cookieTemp = null;
        Cookie[] cookies = request.getCookies();
        if(cookies !=null && cookies.length > 0){
            for (Cookie cookie : cookies){
                if(cookieName.equals(cookie.getName())){
                    cookieTemp = cookie;
                    break;
                }
            }
        }
        return cookieTemp;
    }

    /**
     * 3.获取Cookie的值
     */
    public static String getCookieValue(HttpServletRequest request,String cookieName){
        Cookie cookie = getCookie(request,cookieName);
        return cookie ==null?null:cookie.getValue();
    }
}
