package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@TableName("tb_item_cat")
@Data
@Accessors(chain = true)
public class ItemCat extends BasePojo{
    @TableId(type = IdType.AUTO)
    private Long id;            //Id主键 主键自增
    private Long parentId;      //父级ID
    private String name;        //分类名称
    private Integer status;     //分类状态
    private Integer sortOrder;  //排序信息
    private Boolean isParent;   //是否为父级

}
