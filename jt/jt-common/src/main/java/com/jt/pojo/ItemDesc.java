package com.jt.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@TableName("tb_item_desc")
@Data   //重写toString方法时,只输出自己的属性,父级属性暂时不加
@Accessors(chain = true)
public class ItemDesc extends BasePojo{

    @TableId                    //标识主键  tb_item与tb_Item_Desc ID一致
    private Long itemId;        //商品主键
    private String itemDesc;    //商品详情  html代码片段  大字段进行存储

    /*public String getAbc(){

        return "ABC";
    }

    public void setAbc(String abc){
        System.out.println("哈哈哈 我被调用了 !!!!!");
    }*/
}
