package com.jt.config;

import org.apache.catalina.Host;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import redis.clients.jedis.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration  //标识我是一个配置类   一般和@Bean注解联用
@PropertySource("classpath:/redis.properties")
public class RedisConfig {
    @Value("${redis.nodes}")
    private String nodes;   //node,node,node.....

    @Bean
    public JedisCluster jedisCluster(){
        Set<HostAndPort> nodeSet = new HashSet<>();
        String[] nodeArray = nodes.split(",");
        for (String node : nodeArray){
            String host = node.split(":")[0];
            int port = Integer.parseInt(node.split(":")[1]);
            HostAndPort hostAndPort = new HostAndPort(host, port);
            nodeSet.add(hostAndPort);
        }
        return new JedisCluster(nodeSet);
    }


   /* //配置哨兵机制
    @Value("${redis.sentinel}")
    public String sentinel;

    @Bean
    public JedisSentinelPool jedisSentinelPool(){
        //1.设定连接池大小
        JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMinIdle(5);    //设定最小的空闲数量
        poolConfig.setMaxIdle(10);   //设定最大空闲数量
        poolConfig.setMaxTotal(100); //最大链接数

        //2.链接哨兵的集合
        Set<String> sentinels = new HashSet<>();
        sentinels.add(sentinel);
        return new JedisSentinelPool("mymaster",sentinels,poolConfig);
    }*/


   /* @Value("${redis.nodes}")
    private String nodes;   //指定分片节点  node,node,node.....

    @Bean
    public ShardedJedis shardedJedis(){
        List<JedisShardInfo> shards = new ArrayList<>();
        String[] nodeArray = nodes.split(",");
        for (String node : nodeArray){  //node=host:port
            String host = node.split(":")[0];                   //获取节点IP地址
            int port = Integer.parseInt(node.split(":")[1]);    //获取节点端口号
            JedisShardInfo jedisShardInfo = new JedisShardInfo(host,port);
            shards.add(jedisShardInfo);
        }
        return new ShardedJedis(shards);
    }*/

   /* @Value("${redis.host}")
    private String host;
    @Value("${redis.port}")
    private Integer port;

    @Bean
    public Jedis jedis(){
        return new Jedis(host,port);
    }*/
}
