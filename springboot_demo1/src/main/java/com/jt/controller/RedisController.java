package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RedisController {

    //需求1:能否简化修改的环节

                              // spring将数据扫描到容器中时 才能取
    @Value("${redis.host}")   //spel 表达式
    private String host;      //="192.168.126.111";
    @Value("${redis.port}")
    private int port;         // = 6379;

    @RequestMapping("/getNode")
    public String getNode(){

        return host + "|" +port;
    }

}
